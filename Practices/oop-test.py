class Person():
    def __init__(self, name, occupation):
        self.name = name
        self.occupation = occupation
 
    def getName(self):
        print(self.name)


class Student(Person):
    
    def __init__(self, name, occupation, student_number):
        super().__init__(name, occupation)
        self.student_number = student_number

    def getName(self):
        return self.name + " " + self.student_number

    def getStudentNumber(self):
        return self.student_number


person1 = Person("Spongebob Squarepants", "Fry Cook")
person2 = Person("Lebron James", "Basketball Player")
person3 = Person("Cardo Dalisay", "Immortal Police Officer")
 
print(person1.name)
person2.getName()
print(person3.occupation)

student1 = Student("Serge Rivera", "Student", "1234567")
print(student1.name)
print(student1.getName())
print(student1.getStudentNumber())


