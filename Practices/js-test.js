// declaration of variables
var light = document.getElementById("light");
var dark = document.getElementById("dark");
var body = document.querySelector("body");

// change styles upon pressing of buttons
light.addEventListener("click", changeLight);
dark.addEventListener("click", changeDark);

// function definitions
function changeLight(){
    body.setAttribute("style", "background-color: white; color: black");
}

function changeDark(){
    body.setAttribute("style", "background-color: black; color: white");
}

