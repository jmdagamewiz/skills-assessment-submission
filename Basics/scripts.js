
var username = "rickastley";
var password = "nevergonnagiveyouup";

var usernameField = document.getElementById("name-field");
var passwordField = document.getElementById("password-field");
var loginButton = document.getElementsByClassName("login-button")[0];
var statusLabel = document.getElementsByClassName("status-label")[0];

loginButton.addEventListener("click", Login);

function Login(){
    statusLabel.style.color = "black";
    if (AreInputsValid()){
        if (usernameField.value == username && passwordField.value == password){
            // correct -> logged in
            statusLabel.innerHTML = "Successfully logged in";
            statusLabel.style.backgroundColor = "lightgreen";
        }
        else{
            // incorrect credentials
            statusLabel.innerHTML = "Invalid login credentials";
            statusLabel.style.backgroundColor = "#FFCCCB";
        }
    } else {
        // invalid inputs
        statusLabel.innerHTML = "Invalid input format";
        statusLabel.style.backgroundColor = "#FFCCCB";
    }
}

function AreInputsValid(){
    if (usernameField.value.length < 50 && passwordField.value.length < 50){
        return true;
    } else{
        return false;
    }
}